#ifndef BOLA_H
#define BOLA_H
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>
#include <QPixmap>


class bola: public QGraphicsItem
{
private:
    const double  delta =0.5;
    double posx;
    double posy;
    double angulo;
    double vx_ini;
    double vy_ini;
    int radio;
    bool perdio;
public:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
            QWidget *widget);
    bola();
    void Setbola(double vi_x, double vi_y, double x, double y);
    void setVely(double vy);
    void CalVel();
    void CalPos();
    double getPosy();
    double getPosx();
    double getVely();
    bool getPerdio();
    ~bola();
};

#endif // BOLA_H
