#include "bola.h"
#include <QDebug>

QRectF bola::boundingRect() const
{
   return QRectF(-radio,-radio,2*radio,2*radio);
}

void bola::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::blue);
    painter->drawEllipse(boundingRect());

}

bola::bola()
{
   vx_ini=0;
   vy_ini=0;
   posx=0;
   posy=0;
   angulo=0;
   radio=10;
   perdio=false;
}
void bola::Setbola(double vi_x, double vi_y, double x, double y)
{
    vx_ini=vi_x;
    vy_ini=vi_y;
    posx=x;
    posy=y;
    setPos(x,y);
}

void bola::setVely(double vy)
{
    vy_ini = vy;
}

void bola::CalVel()
{
    vx_ini+= 1*delta;
    vy_ini+= 1*delta;
}

void bola::CalPos()
{
    CalVel();
    if(posx+radio>=509 || posx<=15)
          vx_ini=(-vx_ini);

      if(posy+radio<5)
          vy_ini=(-vy_ini);

      posx+=vx_ini*delta;
      posy+=vy_ini*delta;
      if(posy+radio>=459){
          perdio=true;
      }

      qDebug()<<"posx= "<< posx <<"posy= "<< posy<<endl;
      setPos(posx,posy);
}

double bola::getPosy()
{
   return posy;
}

double bola::getPosx()
{
    return posx;
}

double bola::getVely()
{
    return vy_ini;
}

bool bola::getPerdio()
{
    return perdio;
}

bola::~bola()
{

}
