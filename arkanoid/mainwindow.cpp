#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    h_limit=511;
    v_limit=461;
    timer_mov = new QTimer(this);
    timer_par = new QTimer(this);
    timer_control = new QTimer(this);
    scene=new QGraphicsScene(this);
    scene->setSceneRect(0,0,h_limit,v_limit);
    ui->graphicsView->setScene(scene);
    vidas=3;
    puntaje=0;

    scene->addRect(scene->sceneRect());
    timer_mov->stop();
    timer_par->stop();
    timer_control->stop();
    connect(timer_mov,SIGNAL(timeout()),this,SLOT(mover()));
    connect(timer_par,SIGNAL(timeout()),this,SLOT(par()));
    connect(timer_control,SIGNAL(timeout()),this,SLOT(Joy()));
    c=new grafica();


    c->get_carro()->set_valores(0,0,55,36);
    c->posicion(v_limit);
    scene->addItem(c);
    scene->setFocusItem(c);
    vel=10;
    i=0;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::Iniciar()
{
        Bola= new bola();
        Bola->Setbola(30,30,80,345);
        c->posicion(v_limit);
        scene->addItem(Bola);
        timer_mov->stop();
}

void MainWindow::on_actionGo_triggered()
{

}

void MainWindow::mover()
{
 if(vidas>0)
 {
         Bola->CalPos();
   if(Bola->collidesWithItem(c)){
       double vely = Bola->getVely();
       vely = -vely;
       Bola->setVely(vely);
   }
   if(ladrillos.length()>0)
   {
   for(int i =0; i<ladrillos.length();i++)
   {
       if(Bola->collidesWithItem(ladrillos.at(i)))
        {
           qDebug()<<"colision";
           scene->removeItem(ladrillos.at(i));
           ladrillos.removeAt(i);
           double vely = Bola->getVely();
           vely = -vely;
           Bola->setVely(vely);
           puntaje+=50;
           ui->lcdPuntaje->display(puntaje);
       }

     }
   }
   if(Bola->getPerdio()==true){
       scene->removeItem(Bola);
       vidas--;
       ui->lcdVidas->display(vidas);
       Iniciar();
     }

 }
 else{

 }

}

void MainWindow::par()
{

if (i==0){
c->get_carro()->set_px(0);
c->get_carro()->set_py(200);
i=1;
}
if (c->get_carro()->get_py()>0) c->par(v_limit);
}

void MainWindow::on_actionStop_triggered()
{
    timer_par->stop();
    i=0;
}

void MainWindow::keyPressEvent(QKeyEvent *event){       //Funciones de teclas
    if(event->key()==Qt::Key_C)on_actionGo_triggered();
    if(event->key()==Qt::Key_P)on_actionStop_triggered();
    if(event->key()==Qt::Key_F4) close();
    if(event->key()== Qt::Key_D && c->get_carro()->get_px()<400-(c->get_carro()->get_w()*c->get_escala()))
    {
        c->get_carro()->set_px(c->get_carro()->get_px()+vel);
        on_actionStop_triggered();
        event->accept();
    }
   if(event->key()== Qt::Key_A&& c->get_carro()->get_px()>0)
    {
        c->get_carro()->set_px(c->get_carro()->get_px()-vel) ;
        on_actionStop_triggered();
        event->accept();
    }
   if(event->key()== Qt::Key_M)
    {
       on_actionStop_triggered();
        timer_par->start();
        event->accept();
    }
c->posicion(v_limit);
    }


void MainWindow::on_actionConnect_triggered()
{

}

void MainWindow::Joy(){
        char data;
        int l = 0;
        bool flag=true;
        //while(flag){
           if(control->waitForReadyRead(100)){

                //Data was returned
                l = control->read(&data,1);
                switch (data) {
                case 'A':
                    c->get_carro()->set_py(c->get_carro()->get_py()+vel);
                    on_actionStop_triggered();
                    break;
                case 'B':
                    c->get_carro()->set_px(c->get_carro()->get_px()+vel);
                    on_actionStop_triggered();
                    break;
                case 'C':
                    c->get_carro()->set_py(c->get_carro()->get_py()-vel);
                    on_actionStop_triggered();
                    break;
                case 'D':
                    c->get_carro()->set_px(c->get_carro()->get_px()-vel) ;
                    on_actionStop_triggered();
                    break;
                case 'E':
                    on_actionGo_triggered();
                    break;
                case 'F':
                    on_actionStop_triggered();
                    break;
                case '0':
                    c->get_carro()->set_px(c->get_carro()->get_px()-2*vel) ;
                    on_actionStop_triggered();
                    break;
                case '1':
                    c->get_carro()->set_px(c->get_carro()->get_px()-1.5*vel) ;
                    on_actionStop_triggered();
                    break;
                case '2':
                    c->get_carro()->set_px(c->get_carro()->get_px()-vel) ;
                    on_actionStop_triggered();
                    break;
                case '3':
                    c->get_carro()->set_px(c->get_carro()->get_px()+vel) ;
                    on_actionStop_triggered();
                    break;
                case '4':
                    c->get_carro()->set_px(c->get_carro()->get_px()+1.5*vel) ;
                    on_actionStop_triggered();
                    break;
                case 'X':
                    c->get_carro()->set_px(c->get_carro()->get_px()+2*vel) ;
                    on_actionStop_triggered();
                    break;
                case '5':
                    c->get_carro()->set_py(c->get_carro()->get_py()-2*vel);
                    on_actionStop_triggered();
                    break;
                case '6':
                    c->get_carro()->set_py(c->get_carro()->get_py()-1.5*vel);
                    on_actionStop_triggered();
                    break;
                case '7':
                    c->get_carro()->set_py(c->get_carro()->get_py()-vel);
                    on_actionStop_triggered();
                    break;
                case '8':
                    c->get_carro()->set_py(c->get_carro()->get_py()+vel);
                    on_actionStop_triggered();
                    break;
                case '9':
                    c->get_carro()->set_py(c->get_carro()->get_py()+1.5*vel);
                    on_actionStop_triggered();
                    break;
                case 'Y':
                    c->get_carro()->set_py(c->get_carro()->get_py()+2*vel);
                    on_actionStop_triggered();
                    break;
                case 'H':
                    c->get_carro()->set_py(c->get_carro()->get_py()-vel);
                    c->get_carro()->set_px(c->get_carro()->get_px()-vel) ;
                    on_actionStop_triggered();
                    break;
                case 'Z':
                    c->get_carro()->set_py(c->get_carro()->get_py()-vel);
                    c->get_carro()->set_px(c->get_carro()->get_px()+vel) ;
                    on_actionStop_triggered();
                    break;
                case 'Q':
                    c->get_carro()->set_py(c->get_carro()->get_py()+vel);
                    c->get_carro()->set_px(c->get_carro()->get_px()-vel) ;
                    on_actionStop_triggered();
                    break;
                case 'J':
                    c->get_carro()->set_py(c->get_carro()->get_py()+vel);
                    c->get_carro()->set_px(c->get_carro()->get_px()+vel) ;
                    on_actionStop_triggered();
                    break;
                case 'K':
                    flag=false;
                    timer_control->stop();
                    control->close();
                    //close();
                    break;
                case 'N':
                    flag=false;
                    break;
                default:
                    flag=false;
                    break;
                }
                c->posicion(v_limit);
                qDebug()<<"Response: "<<data;
                flag=false;

            }else{
                //No data
                qDebug()<<"Time out";
          }
        //}
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //control->close();

}

void MainWindow::on_boton_inicio_clicked()
{

}

void MainWindow::on_nivel1_clicked()
{
    Bola= new bola();
    ui->lcdVidas->display(vidas);
    ui->lcdPuntaje->display(puntaje);
    Bola->Setbola(30,30,80,345);
    scene->addItem(Bola);
    ui->nivel1->setEnabled(false);
    ui->nivel2->setEnabled(false);
    ui->nivel3->setEnabled(false);
    int posx=20;
    int posy=30;
    for(int j=0;j<10;j++){

        ladrillos.append(new ladrillo(posx,posy));
        scene->addItem(ladrillos.back());
        posx+=50;
    }
    posx=20;
    posy=50;
    for(int j=0;j<10;j++){

        ladrillos.append(new ladrillo(posx,posy));
        scene->addItem(ladrillos.back());
        posx+=50;
    }
    posx=20;
    posy=70;
    for(int j=0;j<10;j++){

        ladrillos.append(new ladrillo(posx,posy));
        scene->addItem(ladrillos.back());
        posx+=50;
    }
}

void MainWindow::on_nivel2_clicked()
{
    Bola= new bola();
    Bola->Setbola(30,30,80,345);
    scene->addItem(Bola);
    ui->nivel2->setEnabled(false);
    ui->nivel1->setEnabled(false);
    ui->nivel3->setEnabled(false);
    int posx=160;
    int posy=30;
    for(int j=0;j<4;j++){

        ladrillos.append(new ladrillo(posx,posy));
        scene->addItem(ladrillos.back());
        posx+=50;
    }
    posx=60;
    posy=50;
    for(int j=0;j<8;j++){

        ladrillos.append(new ladrillo(posx,posy));
        scene->addItem(ladrillos.back());
        posx+=50;
    }
    posx=60;
    posy=70;
    for(int j=0;j<8;j++){

        ladrillos.append(new ladrillo(posx,posy));
        scene->addItem(ladrillos.back());
        posx+=50;
    }
    posx=160;
    posy=90;
    for(int j=0;j<4;j++){

        ladrillos.append(new ladrillo(posx,posy));
        scene->addItem(ladrillos.back());
        posx+=50;
    }
}

void MainWindow::on_nivel3_clicked()
{

}

void MainWindow::on_BntSalir_clicked()
{

}


void MainWindow::on_btn_inicio_clicked()
{
    timer_mov->start(50);
}

void MainWindow::on_BtnSalir_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Está seguro que desea salir?");
    msgBox.setWindowTitle("Confirmación");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);
   int elegido=msgBox.exec();
   if(elegido==QMessageBox::Yes)
     this->close();
}
