#ifndef CUERPO_H
#define CUERPO_H




class Cuerpo
{
private:
    float px;
    float py;
    float h=40;
    float w=40;
    float dt=0.1;

public:
    Cuerpo();

    void set_valores(float x, float y, float a, float n);
    void par();
    float get_px();

    float get_py();

    float get_h();

    float get_w();


    void set_px(float x);

    void set_py(float y);
};

#endif // CUERPO_H
