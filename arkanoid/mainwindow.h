#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include <QFile>
#include <QList>
#include "cuerpo.h"
#include "cuerpograf.h"
#include "bola.h"
#include "ladrillo.h"
#include "obstaculo.h"
#include <QKeyEvent>
#include <QtSerialPort/QSerialPort>
#include <QFileDialog>
#include <QDebug>
#include <QTextStream>
#include <QtSerialPort/QSerialPortInfo>
#include <QFile>
#include <QMessageBox>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QSerialPort *control;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Iniciar();
private slots:
    void keyPressEvent(QKeyEvent *event);
    void on_actionGo_triggered();
    void mover();
    void par();
    void on_actionStop_triggered();
    void Joy();
    void on_actionConnect_triggered();

    void on_boton_inicio_clicked();

    void on_nivel1_clicked();

    void on_nivel2_clicked();

    void on_nivel3_clicked();

    void on_BntSalir_clicked();

    void on_BtnInicio_clicked();

    void on_btn_inicio_clicked();

    void on_BtnSalir_clicked();

private:
    Ui::MainWindow *ui;
    int h_limit;                //longitud en X del mundo
    int v_limit;                //longitud en Y del mundo
    int vel;
    bool i=0;
    int vidas;
    int puntaje;
    int nivel;
    int nivelcount;
    QTimer *timer_mov;
    QTimer *timer_par;
    QTimer *timer_control;
    QList <ladrillo*> ladrillos;
    QList <obstaculo*> ladrillosDuros;


    QGraphicsScene *scene;
    grafica *c;
    bola *Bola;
    ladrillo *Ladrillo;
};

#endif // MAINWINDOW_H
