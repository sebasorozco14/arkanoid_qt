#include "obstaculo.h"

obstaculo::obstaculo()
{
    r=10;
    posx=0;
    posy=0;
}

obstaculo::obstaculo(int x, int y)
{
    r=15;
    posx=x;
    posy=y;
    setPos(posx, posy);
}

QRectF obstaculo::boundingRect() const
{
      return QRectF(-r,-r,3*r,r);
}

void obstaculo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::white);
    painter->drawRect(boundingRect());
}
