#include "ladrillo.h"

ladrillo::ladrillo()
{
    r=10;
    posx=0;
    posy=0;
}

ladrillo::ladrillo(int x, int y)
{
    r=15;
    posx=x;
    posy=y;
    setPos(posx, posy);
}

QRectF ladrillo::boundingRect() const
{
      return QRectF(-r,-r,3*r,r);
}

void ladrillo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::red);
    painter->drawRect(boundingRect());
}

int ladrillo::getR()
{
    return r;
}
