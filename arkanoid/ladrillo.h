#ifndef LADRILLO_H
#define LADRILLO_H
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>
#include <QPixmap>

class ladrillo : public QGraphicsItem
{
    int r;
    double posx;
    double posy;
public:
    ladrillo();
    ladrillo(int x, int y);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
            QWidget *widget);
    int getR();
};

#endif // LADRILLO_H
