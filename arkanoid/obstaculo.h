#ifndef OBSTACULO_H
#define OBSTACULO_H
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>

class obstaculo: public QGraphicsItem
{
    int r;
    double posx;
    double posy;
public:
    obstaculo();
    obstaculo(int x, int y);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
            QWidget *widget);
};

#endif // OBSTACULO_H
