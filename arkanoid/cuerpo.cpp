#include "cuerpo.h"

Cuerpo::Cuerpo()
{
    px=0;
    py=0;
    h=0;
    w=0;
}
void Cuerpo::set_valores(float x, float y, float a, float n)
{
  px=x;
  py=y;
  h=a;
  w=n;
}
float Cuerpo::get_px()
{
  return px;
}
float Cuerpo::get_py()
{
  return py;
}
float Cuerpo::get_h()
{
  return h;
}
float Cuerpo::get_w()
{
  return w;
}
void Cuerpo::set_px(float x)
{
  px=x;
}
void Cuerpo::set_py(float y)
{
  py=y;
}
void Cuerpo::par()
{

        px=px+dt;
        py=py+dt-0.2;

}
