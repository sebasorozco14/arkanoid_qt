/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionconnect;
    QAction *actionstop;
    QWidget *centralWidget;
    QPushButton *btn_inicio;
    QGraphicsView *graphicsView;
    QLCDNumber *lcdVidas;
    QLCDNumber *lcdPuntaje;
    QLabel *label;
    QLabel *label_2;
    QPushButton *nivel1;
    QPushButton *nivel2;
    QPushButton *nivel3;
    QPushButton *BtnSalir;
    QMenuBar *menuBar;
    QMenu *menuoptions;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(804, 556);
        actionconnect = new QAction(MainWindow);
        actionconnect->setObjectName(QStringLiteral("actionconnect"));
        actionstop = new QAction(MainWindow);
        actionstop->setObjectName(QStringLiteral("actionstop"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        btn_inicio = new QPushButton(centralWidget);
        btn_inicio->setObjectName(QStringLiteral("btn_inicio"));
        btn_inicio->setGeometry(QRect(640, 370, 91, 31));
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        graphicsView->setGeometry(QRect(10, 10, 541, 491));
        lcdVidas = new QLCDNumber(centralWidget);
        lcdVidas->setObjectName(QStringLiteral("lcdVidas"));
        lcdVidas->setGeometry(QRect(680, 40, 81, 31));
        lcdPuntaje = new QLCDNumber(centralWidget);
        lcdPuntaje->setObjectName(QStringLiteral("lcdPuntaje"));
        lcdPuntaje->setGeometry(QRect(680, 90, 81, 31));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(590, 40, 71, 31));
        QFont font;
        font.setFamily(QStringLiteral("Tempus Sans ITC"));
        font.setPointSize(14);
        label->setFont(font);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(590, 90, 71, 31));
        label_2->setFont(font);
        nivel1 = new QPushButton(centralWidget);
        nivel1->setObjectName(QStringLiteral("nivel1"));
        nivel1->setGeometry(QRect(640, 170, 91, 31));
        nivel2 = new QPushButton(centralWidget);
        nivel2->setObjectName(QStringLiteral("nivel2"));
        nivel2->setGeometry(QRect(640, 220, 91, 31));
        nivel3 = new QPushButton(centralWidget);
        nivel3->setObjectName(QStringLiteral("nivel3"));
        nivel3->setGeometry(QRect(640, 270, 91, 31));
        BtnSalir = new QPushButton(centralWidget);
        BtnSalir->setObjectName(QStringLiteral("BtnSalir"));
        BtnSalir->setGeometry(QRect(590, 460, 75, 23));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 804, 21));
        menuoptions = new QMenu(menuBar);
        menuoptions->setObjectName(QStringLiteral("menuoptions"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuoptions->menuAction());
        menuoptions->addAction(actionconnect);
        menuoptions->addAction(actionstop);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionconnect->setText(QApplication::translate("MainWindow", "connect", Q_NULLPTR));
        actionstop->setText(QApplication::translate("MainWindow", "stop", Q_NULLPTR));
        btn_inicio->setText(QApplication::translate("MainWindow", "Iniciar", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "vidas:", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "puntaje:", Q_NULLPTR));
        nivel1->setText(QApplication::translate("MainWindow", "Nivel 1", Q_NULLPTR));
        nivel2->setText(QApplication::translate("MainWindow", "Nivel 2", Q_NULLPTR));
        nivel3->setText(QApplication::translate("MainWindow", "Nivel 3", Q_NULLPTR));
        BtnSalir->setText(QApplication::translate("MainWindow", "salir", Q_NULLPTR));
        menuoptions->setTitle(QApplication::translate("MainWindow", "options", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
